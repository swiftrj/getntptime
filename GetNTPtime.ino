#include "NTPTimeLib.h"
#include "secret.h"

#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>

ESP8266WiFiMulti WiFiMulti;

void setup() {
  Serial.begin(9600); delay(100);
  Serial.println("----Setup()----");

  Serial.println("----WIFI----");
  // We start by connecting to a WiFi network
  WiFi.mode(WIFI_STA);
  WiFiMulti.addAP(_SSID, _PASS);
  while (WiFiMulti.run() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println(".");
  Serial.println("WIFI Connected!\n");
  Serial.print("IP address  : "); Serial.println(WiFi.localIP());
  Serial.print("MAC Address : "); Serial.println(WiFi.macAddress());
  Serial.print("SubNet Mask : "); Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP  : "); Serial.println(WiFi.gatewayIP());
  Serial.print("Hostname    : "); Serial.println(WiFi.hostname());
  Serial.print("Status      : "); Serial.println(WiFi.status());
  Serial.print("SSID        : "); Serial.println(WiFi.SSID());
  Serial.print("PSK         : "); Serial.println(WiFi.psk());
  Serial.print("BSSID       : "); Serial.println(WiFi.BSSIDstr());
  Serial.print("RSSI        : "); Serial.println(WiFi.RSSI());
  Serial.println("====WIFI====");

}

void loop() {
  // put your main code here, to run repeatedly:
}
